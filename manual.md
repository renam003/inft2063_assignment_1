**User Manual for Cash Register**

How to download the software:
-----------------------------


Download [git](https://git-scm.com/downloads) for this software, and follow the install directions on the website to install it.


Open git.bash

Select where you want the software to be downloaded to , by using the command 

>cd /directoryname/ <-- The directory name being the name of the directory you wish to enter.

If you are unsure what is in the current directory use the command 

>ls

This will show you a list of all the files in the current directory that you are in.

Once in the directory that is desired, create a new folder in that directory, this is where the files for the software will be stored.

Once done, go back into the git bash use the command 

>cd newfolder   <-- replacing new folder with the folders name you just created.

Once inside the folder, now its time to download the software, with the command;

>git pull https://gitlab.com/maemokaihatsusha/inft2063_assignment_1.git

This will then start a short download, pulling the software into the newly created folder

Installing the software
-----------------------
 
Once downloaded, you will also need something to run the java code with.
Here are three good compilers that would be suitable for the code

>[Eclipse](https://www.eclipse.org/)   
>[NetBeans](https://netbeans.org/)   
>[Intellij](https://www.jetbrains.com/idea/features/)

Once installed one of the provided applications, open the software, and create a new package, once the package has been created drag the contents from the source file from the downloaded git folder into it, besides from the makefile. The makefile gets put into the project’s folder. 

Using the software
------------------
Once these previous steps have been completed, the program can then be run, this is done various different ways depending on what compiler you are using. In eclipse it is done by pressing the green button of some sort.

Once ran, it will open up a text based program, this program being a cash register app with transactions. It uses your input from a keyboard to make a purchase of a selected item.

Firstly the application will ask you to enter the cash register's balance, this will determine how much cash is in the register when before a product is purchased. Inputting anything but a number will cause it to keep looping saying incorrect input, much like the rest of the program when any input is wrong.

A transaction is started by either an input of a 'y' representing a yes, or a 'n' representing a no. A no will stop the program and a yes will continue. The program will then ask for the items name, and the price of the item, after these are inputted, it will then ask if you want to continue with the transaction, furthermore adding items or stopping the transaction and proceeding to checkout.

Once in the checkout, a loyalty card prompt will ask the customer if they have a loyalty card, the total amount will be shown to you, and the application will then ask you to pay by entering a selected amount. If the amount is over you will get change back , just like any normal cash register in real life. However if the amount is under the total the application will stop. After enter it will display a receipt, followed by a "Thank you for using our service!", and the balance of the register.

This application will then stop as the purchase has been made. If you want to make another purchase just repeat the steps from the start of this description.
