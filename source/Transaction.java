public class Transaction {
	private int qty;
	private String name;
	private double cost;
	
	public Transaction(int qty, String name, double cost) {
		this.qty = qty;
		this.name = name;
		this.cost = cost;
	}
	
	public int getQty() {
		return qty;
	}
	
	public void setQty(int qty) {
		this.qty = qty;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getCost() {
		return cost;
	}
	
	public void setCost(double cost) {
		this.cost = cost;
	}
}
