import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class CashRegister {
    // Initialize variables.
	private static boolean execute = true;
	private static double balance, change, tendered, totalCost = 0;
	// Transactions list -
	private static ArrayList<Transaction> transactionsList = new ArrayList<>();
	// Scanner -
	private static Scanner in = new Scanner(System.in);
	// Receipt -
	private static Receipt rec;
	
	public static void main(String[] args) {
		int qty;
		String name;
		double cost;
	    // Warm welcome message ;)
		System.out.println("Hello. Welcome to cash register.");
		// Balance prompt.
		balance = numberDoublePrompt("Please enter the Cash Register's balance: $");
		// Initialize transaction?
		responsePrompt("\n*** Start transaction? (Y/N) ***: ", false, true);
		
		// Add item loop.
		if (execute) {
			do {
			    // Prompt transaction details, create and add transaction to transactions list.
				name = stringPrompt("Please enter the item's name: ");
				cost = numberDoublePrompt("Please enter the item's cost (each): $");
				qty = numberIntPrompt("Please enter the item's quantity: ");
				transactionsList.add(new Transaction(qty, name, cost));
				// Continue to next item?
				responsePrompt("\n*** Next item? (Y/N) ***: ", false, true);
			} while (execute);
			
			// Calculate total costs.
			for (Transaction trans : transactionsList) {
				totalCost += trans.getQty() * trans.getCost();
			}
			
			totalCost = discountPrice("\nPlease enter the discounted percentage: ");
			loyaltyCard(totalCost);
			System.out.println("TOTAL: $" + totalCost);
			// Prompt tendered amount.
			tendered = numberDoublePrompt("\nPlease enter the cash amount tendered: $");
			

			// Check if tendered amount is sufficient to cover the cost.
			if (tendered >= totalCost) {
			    // Update cashier balance.
				balance += totalCost;
				// Calculate and display change.
				change = tendered - totalCost;
				System.out.println("\nAmount of change is $" + change + ".");
				rec = new Receipt(transactionsList,totalCost,tendered,change);
				// Prompt for receipt.
				responsePrompt("\n*** Would you like a receipt? (Y/N) ***: ", true, false);
			} else {
			    // Do nothing, warn insufficient fund.
				System.out.println("\n*** Insufficient fund! Minimum $" + totalCost + " required. ***");
			}
		}
		
		// Close string scanner.
		in.close();
		// Warm leaving message :)
		System.out.println("\nThank you for using our service!");
		System.out.println("Current balance of this cash register: $" + balance);
	}
	
	/* Number prompt - prompts number (double) by displaying input string and returns a double
	 * --- --- ---
	 * Includes input type validation.
	 * Returns: double
	 */
	private static double numberDoublePrompt (String prompt) {
		boolean valid;
		double number = 0;
		do {
			valid = true;
			System.out.print(prompt);
			try {
				number = Double.parseDouble(in.nextLine());
			} catch (NumberFormatException e){
				valid = false;
				System.out.println("\n*** Invalid input! ***\n");
				}
			if (number < 0) {
				valid = false;
				System.out.println("\n*** Invalid input! ***\n");
			} 
			
		} while (!valid);
		return number;
	}
	
	/* Number prompt - prompts number (int) by displaying input string and returns a integer
	 * --- --- ---
	 * Includes input type validation.
	 * Returns: int
	 */
	private static int numberIntPrompt(String prompt) {
		boolean valid;
		int number = 0;
		do {
			valid = true;
			System.out.print(prompt);
			try {
				number = Integer.parseInt(in.nextLine());
			} catch (NumberFormatException e) {
				valid = false;
				System.out.println("\n*** Invalid input! ***\n");
			}
		} while (!valid);
		return number;
	}
	
	/* Response prompt - a clumsy receipt / continue loop prompt
	 * --- --- ---
	 * Includes input type validation for receipt and loop execution.
	 * Returns: void / Nothing
	 */
	// TO-DO: Please rewrite the manipulation bits into separate functions!
	private static void responsePrompt(String prompt, boolean injectReceipt, boolean noExecution) {
		boolean valid;
		String response;
		do {
			valid = true;
			System.out.print(prompt);
			response = in.nextLine();
			switch (response.toLowerCase()) {
				case "y":
					if (injectReceipt) rec.printReceipt();
					break;
				case "n":
					if (noExecution) execute = false;
					break;
				default:
					valid = false;
					System.out.println("\n*** Invalid input! ***");
					break;
			}
		} while (!valid);
	}
	
	// String prompt - a basic string prompt
	// Returns: String
	private static String stringPrompt (String prompt) {
		System.out.print(prompt);
		return in.nextLine();
	}
	
	private static double discountPrice(String prompt) {
		double discount;
		System.out.println("\nThe current total is: " + totalCost);
		discount = numberDoublePrompt(prompt);
		discount = (100- discount)/100 * totalCost;
		System.out.println("\nThe new total is: " + discount);
		return discount;
	}
	/** 
	 * Method to prompt and valid user input.
	 * @param prompt Prompt message to the user
	 * @return The input from the user
	 */
	private static String responsePrompt(String prompt) {
		boolean valid;
		String response;
		do {
			valid = true;
			System.out.print(prompt);
			response = in.nextLine();
			switch (response.toLowerCase()) {
				case "y":
					return response;
				case "n":
					return response;
				default:
					valid = false;
					System.out.println("\n*** Invalid input! ***");
					break;
			}
		} while (!valid);
		return response;
	}
	
	/**
	 * A feature that allows customers to use their loyalty card. 
	 * Due to limitations the total points on a customers card cannot be tracked
	 * hence, they the total points on their card will always be the same as
	 * points earned
	 * 
	 * @param cost The total cost of the transaction
	 */
	public static void loyaltyCard(double cost) {
		int cardNum;
		String userInput;
		userInput = responsePrompt("Do you have a loyalty card? (Y/N)");
		
		switch(userInput.toLowerCase()) {
		case "y":
			System.out.println("\n*** loyalty card ***");
			System.out.println("\nplease enter card number (9 Digits)");
			
			cardNum = in.nextInt();
			int loyalPoints = (int) cost;
			
			System.out.println("Points earned from this shop:" + loyalPoints);
			System.out.println("Total points on " + cardNum + ": " + loyalPoints);
			break;
		case "n":
			System.out.println("Please consider joining our loyalty program, "
					+ "you may sign up at the services desk");
			break;
		default:
			break;
		}
	}
}
